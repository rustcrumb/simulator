#include <cstddef>
#include <vector>

#include "sim_raft_persistence.h"

using namespace std;

sim_raft_persistence::sim_raft_persistence() :
	m_log(1) {

}

term_t
sim_raft_persistence::get_term() {
	return m_term;
}

server_id_t
sim_raft_persistence::get_voted_for() {
	return m_voted_for;
}

log_index_t
sim_raft_persistence::max_log_index() {
	return m_log[m_log.size() - 1]->index;
}

term_t
sim_raft_persistence::max_log_term() {
	return m_log[m_log.size() - 1]->term;
}

log_entry_t*
sim_raft_persistence::get_log_entry(log_index_t index) {
	if(m_log.size() <= index) {
		return NULL;
	}

	return m_log[index];
}

void
sim_raft_persistence::set_term(term_t term) {
	m_term = term;
}

void
sim_raft_persistence::set_voted_for(server_id_t peer) {
	m_voted_for = peer;
}

void
sim_raft_persistence::delete_entries(log_index_t starting_index) {
	m_log.erase(m_log.begin() + starting_index, m_log.end());
}

void
sim_raft_persistence::append_entry(log_entry_t* entry) {
	m_log.push_back(entry);
}

