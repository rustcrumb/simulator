#include <sstream>
#include <queue>
#include <vector>
#include "sim_types.h"
#include "simulator.h"
#include "test_actor.h"
#include "test_observer.h"
#include "sim_raft_server.h"


using namespace std;
using namespace sim;

/**
 *	simple set of actors and observers to exercise simulation code
 */
void test() {
	simulator s;

	s.add_observer(new test_observer());
	int num_actors = 1000;
	for(int i = 1; i < num_actors; ++i) {
		stringstream ss;
		ss<<"test"<<i;
		s.add_actor(new test_actor(ss.str(), i/100 + 1));
	}

	s.run_until(num_actors * 10);
}

/**
 *	Run a simulation of a bunch of Raft servers talking to each other.
 *	todo: add more servers
 *	todo: add observer to verify raft invariants are maintained
 *	todo: add client actors to simulate additions to replicated log
 *	todo: measure performance of log replication somehow
 */
void test_raft() {
	simulator s;
	s.add_actor(new sim_raft_server());

	s.run_until(100000);
}

int main(int argc, const char* argv[]) {
	test_raft();

	return 0;
}
