#ifndef ITIMER_H
#define ITIMER_H

namespace raft {
	class itimer;
}

#include "raft_types.h"

namespace raft {
class itimer {
	public:
		virtual server_time_t time() = 0;
		virtual void schedule_expiration(server_duration_t duration) = 0;
};

}
#endif
