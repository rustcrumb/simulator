#ifndef SIM_RAFT_PERSISTNECE_H
#define SIM_RAFT_PERSISTNECE_H

#include <vector>

#include "ipersistence.h"
#include "raft_types.h"

using namespace std;
using namespace raft;

class sim_raft_persistence: public ipersistence {
	public:
		sim_raft_persistence();

		virtual term_t get_term();
		virtual server_id_t get_voted_for();

		virtual log_index_t max_log_index();
		virtual term_t max_log_term();
		
		//todo: not very happy with returning a pointer
		virtual log_entry_t* get_log_entry(log_index_t index);

		virtual void set_term(term_t term);
		virtual void set_voted_for(server_id_t peer);
		virtual void delete_entries(log_index_t starting_index);
		virtual void append_entry(log_entry_t* entry);

	private:
		term_t m_term;
		server_id_t m_voted_for;
		vector<log_entry_t*> m_log;
};

#endif
