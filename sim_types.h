#ifndef SIM_TYPES_H
#define SIM_TYPES_H

namespace sim {
typedef unsigned long sim_time_t;
typedef unsigned long sim_duration_t;
typedef unsigned int sim_event_t;
}

#endif

