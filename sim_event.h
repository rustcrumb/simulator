#ifndef SIM_EVENT
#define SIM_EVENT

namespace sim {
	class sim_event;
}

#include "iactor.h"

namespace sim {
class sim_event {
	public:
		sim_event(iactor* actor, sim_time_t time, sim_event_t type);
		
		iactor* actor() const;
		sim_time_t time() const;
		sim_event_t type() const;

		void cancel();

		bool operator==(const sim_event& rhs) const {
			return this->time() == rhs.time();
		}

		bool operator!=(const sim_event& rhs) const {
			return !(*this == rhs);
		}

		bool operator< (const sim_event& rhs) const {
			return this->time() < rhs.time(); 
		}

		bool operator> (const sim_event& rhs) const {
			return  rhs < *this;
		}

		bool operator<=(const sim_event& rhs) const {
			return !(*this > rhs);
		}

		bool operator>=(const sim_event& rhs) const {
			return !(*this < rhs);
		}

	private:
		bool m_valid;
		iactor* m_actor;
		sim_time_t m_time;
		sim_event_t m_type;
};

}
#endif

