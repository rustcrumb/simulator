#include "sim_event.h"

namespace sim {

sim_event::sim_event(iactor* actor, sim_time_t time, sim_event_t type) 
	: m_actor(actor), m_time(time), m_type(type) {

}

iactor*
sim_event::actor() const {
	return m_actor;
}

sim_time_t
sim_event::time() const {
	return m_time;
}

sim_event_t
sim_event::type() const {
	return m_type;
}

}

