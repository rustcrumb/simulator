#ifndef SIM_RAFT_SERVER_H
#define SIM_RAFT_SERVER_H

#include "iactor.h"
#include "itimer.h"
#include "irpc.h"
#include "ipersistence.h"

#include "simulator.h"
#include "sim_types.h"

#include "raft_server.h"

using namespace sim;
using namespace raft;

/**
 *	class for running a raft server in a simulation
 */
class sim_raft_server: public iactor, public itimer{
	public:
		sim_raft_server();

		// iactor
		virtual void start(simulator* sim);
		virtual void handle_event(sim_event_t event, simulator* sim);

		//itimer
		virtual server_time_t time();
		virtual void schedule_expiration(server_duration_t duration);

	private:
		raft_server * m_server;
		simulator* m_sim;

		//event types
		static const sim_event_t ELECTION_TIMEOUT = 1;
};

#endif

