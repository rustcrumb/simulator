#ifndef TEST_OBSERVER_H
#define TEST_OBSERVER_H

#include "iobserver.h"
#include "simulator.h"

using namespace sim;
class test_observer: public iobserver {
	public:
		test_observer();
		virtual void observe(simulator* sim);
	private:
		sim_time_t m_last_printed_time;
};

#endif

