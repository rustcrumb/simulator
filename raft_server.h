#ifndef RAFT_SERVER_H
#define RAFT_SERVER_H

namespace raft {
	class raft_server;
}

#include <map>
#include <vector>
#include "raft_types.h"
#include "itimer.h"
#include "irpc.h"
#include "ipersistence.h"
using namespace std;

namespace raft {

class raft_server {
	public:
		raft_server(int random_seed, itimer* timer, irpc* rpc, ipersistence* persist);

		//todo: returning a result from these requests
		request_vote_response_t recv_request_vote(
			term_t candidate_term, 
			server_id_t candidate_id, 
			log_index_t last_log_index,
			term_t last_log_term);

		append_entries_response_t recv_append_entries(
			term_t leader_term,
			server_id_t leader_id,
			log_index_t prev_log_index,
			term_t prev_log_term,
			vector<log_entry_t*>& entries,
			log_index_t commit_index);

		void start();
		void election_timeout();

	private:
		int m_seed;
		itimer* m_timer;
		irpc* m_rpc;
		ipersistence* m_persistence;

		enum server_state { SERVER_FOLLOWER, SERVER_CANDIDATE, SERVER_LEADER };
		server_state m_state;

		//todo: log object

		//NB: volatile state members
		server_time_t m_last_rpc_time;
		server_duration_t m_election_timeout;

		log_index_t m_commit_index;
		log_index_t m_last_applied;

		//only used when server_state == SERVER_LEADER

		//for each server, index of the next log entry to send to that server
		map<server_id_t, log_index_t> m_next_indices;

		//for each server, index of highest log entry known to be replicated on server
		map<server_id_t, log_index_t> m_match_indices;

		
		void start_election();
		void update_term_and_state(term_t term);
		int rand(int min, int max);
};

}
#endif
