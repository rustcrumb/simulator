#ifndef IRPC_H
#define IRPC_H

#include <vector>

using namespace std;

namespace raft {


class irpc {
	public:
		static const server_id_t nobody = 0;

		virtual void send_request_vote(
				term_t candiate_term, 
				server_id_t candidate_id, 
				log_index_t last_log_index,
				term_t last_log_term) = 0;

		virtual void send_append_entries(
				term_t leader_term,
				server_id_t leader_id,
				log_index_t prev_log_index,
				term_t prev_log_term,
				vector<log_entry_t> entries,
				log_index_t commit_index) = 0;

};

}
#endif
