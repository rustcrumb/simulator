#ifndef SIMULATOR_H
#define SIMULATOR_H

namespace sim {
	class simulator;
}

#include <queue>
#include <vector>
#include "sim_types.h"
#include "sim_event.h"
#include "iactor.h"
#include "iobserver.h"


namespace sim {
class simulator {
	public:
		simulator();

		//todo: these should be passed in to the constructor
		void add_actor(iactor* actor);
		void add_observer(iobserver* observer);

		sim_time_t time() const;

		void run_until(sim_time_t time);

		void schedule_event(iactor* actor, sim_duration_t duration, sim_event_t type);

	private:
		sim_time_t m_time;
		std::priority_queue<sim_event, std::vector<sim_event>, std::greater<sim_event> > m_events;
		std::vector<iactor*> m_actors;
		std::vector<iobserver*> m_observers;
};
}

#endif

