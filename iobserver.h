#ifndef IOBSERVER_H
#define IOBSERVER_H
 
namespace sim {
	class iobserver;
}

#include "simulator.h"

namespace sim {
class iobserver {
	public:
		virtual void observe(simulator* sim) = 0;
};
}

#endif
