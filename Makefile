#super basic makefile

GCC = g++ -ansi -Wall -pedantic-errors -g
BIN = simulator.out
OBJ = main.o \
simulator.o \
sim_event.o \
test_actor.o \
test_observer.o \
raft_server.o \
sim_raft_server.o \
sim_raft_persistence.o

DEP = $(OBJ:.o=.d)

$(BIN): $(OBJ)
	$(GCC) $^ -o $@

%.o: %.cpp
	$(GCC) $< -c

.PHONY: test
test: $(BIN)
	time ./$(BIN)

.PHONY: clean
clean:
	rm -f $(DEP) $(OBJ) $(BIN)

%.d : %.cpp
	$(GCC) -M $< | sed 's/\($*\)\.o[ :]*/\1.o $@: /g' > $@

include $(DEP)
