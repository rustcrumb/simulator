#ifndef IACTOR_H
#define IACTOR_H

namespace sim {
	class iactor;
}

#include "simulator.h"
#include "sim_types.h"

namespace sim {

/**
 *	Interface to be implemented by "actors" in the simulation.
 *	Actors use a simulator to schedule future callbacks.
 *	The simulator will call handle_event on actors when the time in the simulation
 *  has reached the time of an event scheduled via schedule_event
 */
class iactor {
	public:
		virtual void start(simulator* sim) = 0;
		virtual void handle_event(sim_event_t type, simulator* sim) = 0;
};
}

#endif

