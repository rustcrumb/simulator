#include <cstddef>
#include <cstdlib>

#include "raft_server.h"
#include "ipersistence.h"

using namespace std;
namespace raft {

/**
 *	Implementation of "Raft" consensus/distributed log system.
 *	See: https://ramcloud.stanford.edu/wiki/download/attachments/11370504/raft.pdf
 *
 *	The goal of this implementation is to encapsulate the raft behavior and logic here,
 *	without specifying lower-level details such as the RPC, persistent storage, or time-keeping
 *	mechanisms to use.
 */

/**
 *	Raft uses randomized timeouts for each server to help trigger and resolve 
 * 	leader elections in a timely fashion.
 */
const unsigned int MIN_ELECTION_TIMEOUT_MS = 150;
const unsigned int MAX_ELECTION_TIMEOUT_MS = 300;

raft_server::raft_server(int random_seed, itimer* timer, irpc* rpc, ipersistence* persist) 
	:	m_seed(random_seed),
		m_timer(timer),
		m_rpc(rpc),
		m_persistence(persist),
		m_state(SERVER_FOLLOWER),
		m_last_rpc_time(0),
		m_commit_index(0) {
}

void
raft_server::start() {
	m_election_timeout = this->rand((int)MIN_ELECTION_TIMEOUT_MS, (int)MAX_ELECTION_TIMEOUT_MS);
	m_timer->schedule_expiration(m_election_timeout);
}

request_vote_response_t
raft_server::recv_request_vote(
	term_t candidate_term, 
	server_id_t candidate_id, 
	log_index_t last_log_index,
	term_t last_log_term) {

	update_term_and_state(candidate_term);
	server_id_t voted_for = m_persistence->get_voted_for();

	request_vote_response_t ret;
	ret.term = m_persistence->get_term();
	ret.vote_granted = false;

	 if((voted_for == irpc::nobody || voted_for == candidate_id) &&
		last_log_term >= m_persistence->max_log_term() &&
		last_log_index >= m_persistence->max_log_index()) {

	 	m_persistence->set_voted_for(candidate_id);
	 	ret.vote_granted = true;
	 }

	return ret;
}

append_entries_response_t
raft_server::recv_append_entries(
	term_t leader_term,
	server_id_t leader_id,
	log_index_t prev_log_index,
	term_t prev_log_term,
	vector<log_entry_t*>& entries,
	log_index_t commit_index) {

	update_term_and_state(leader_term);

	term_t term = m_persistence->get_term();

	append_entries_response_t ret;

	ret.term = term;
	ret.success = true;

	if(leader_term < term || prev_log_index > m_persistence->max_log_index()) {
		ret.success = false;
	} else {
		log_entry_t* prev_entry = m_persistence->get_log_entry(prev_log_index);
		if(prev_log_term != prev_entry->term) {
			ret.success = false;
		}
	}

	if(!ret.success) {
		return ret;
	}

	for(int i = 0, n = entries.size(); i < n; ++i) {
		log_entry_t* my_entry = m_persistence->get_log_entry(entries[i]->index);
		//todo: should this be "not equal" or "less than" or what?
		if(my_entry != NULL || my_entry->term != entries[i]->term) {
			m_persistence->delete_entries(entries[i]->index);
			break;
		}
	}

	//todo: think about gaps in logs occurring
	for(int i = 0, n = entries.size(); i < n; ++i) {
		if(entries[i]->index > m_persistence->max_log_index()) {
			m_persistence->append_entry(entries[i]);
		}
	}

	m_commit_index = min(commit_index, m_persistence->max_log_index());

	return ret;
}

void
raft_server::election_timeout() {
 	if(m_timer->time() >= m_last_rpc_time + m_election_timeout) {
 		start_election();
 		m_timer->schedule_expiration(m_election_timeout);
 	}
 	else {
 		m_timer->schedule_expiration(m_election_timeout - (m_timer->time() - m_last_rpc_time));
 	}
}

void
raft_server::start_election() {
	m_persistence->set_term(m_persistence->get_term()+1);
	m_state = SERVER_CANDIDATE;

	//todo: send RPC to every other server
}

void
raft_server::update_term_and_state(term_t peer_term) {
	if(peer_term > m_persistence->get_term()) {
		m_persistence->set_term(peer_term);
		m_state = SERVER_FOLLOWER;
	}
}

int
raft_server::rand(int min, int max) {
	srand(m_seed);
	m_seed = std::rand();
	return min + m_seed % (max - min);
}
}
