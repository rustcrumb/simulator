#ifndef TEST_ACTOR_H
#define TEST_ACTOR_H

#include <string>
#include "iactor.h"
using namespace sim;
using namespace std;
class test_actor: public iactor {
public:
	test_actor(string id, sim_duration_t frequency);
	void start(simulator* sim);
	void handle_event(sim_event_t type, simulator* sim);
	
private:
	string m_id;
	sim_duration_t m_freq;
	void log(string msg);
};
#endif

