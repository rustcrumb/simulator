#ifndef IPERSISTENCE_H
#define IPERSISTENCE_H

#include "raft_types.h"

namespace raft {

class ipersistence {
	public:
		virtual term_t get_term() = 0;
		virtual server_id_t get_voted_for() = 0;

		virtual log_index_t max_log_index() = 0;
		virtual term_t max_log_term() = 0;
		
		//todo: not very happy with returning a pointer
		virtual log_entry_t* get_log_entry(log_index_t index) = 0;

		virtual void set_term(term_t term) = 0;
		virtual void set_voted_for(server_id_t peer) = 0;
		virtual void delete_entries(log_index_t starting_index) = 0;
		virtual void append_entry(log_entry_t* entry) = 0;
};
}
#endif
