#include <iostream>
#include <cstddef>
#include "sim_raft_server.h"
#include "sim_types.h"
#include "raft_types.h"
#include "sim_raft_persistence.h"

using namespace std;
using namespace sim;
using namespace raft;

sim_raft_server::sim_raft_server() {
	m_server = new raft_server(
		1, //todo: provide IDs
		this,
		NULL, //irpc* rpc, 
		new sim_raft_persistence()
	);
}

void
sim_raft_server::start(simulator* sim) {
	cout<<"started"<<endl;
	m_sim = sim;
	m_server->start();
}

void
sim_raft_server::handle_event(sim_event_t event, simulator* sim) {
	cout<<"handle event"<<endl;
	if(event == ELECTION_TIMEOUT) {
		cout<<"Election timeout: "<<m_sim->time()<<endl;
		m_server->election_timeout();
	}
}


server_time_t
sim_raft_server::time() {
	return m_sim->time();
}

void
sim_raft_server::schedule_expiration(server_duration_t duration) {
	cout<<"Scheduling expiration. time="<<m_sim->time()<<" duration="<<duration<<endl;
	m_sim->schedule_event(this, duration, ELECTION_TIMEOUT);
}

