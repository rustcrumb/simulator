#ifndef SIM_RAFT_RPC_H
#define SIM_RAFT_RPC_H

#include "irpc.h"

class sim_raft_rpc {
	public:
		sim_raft_rpc();

		virtual void send_request_vote(
				term_t candiate_term, 
				server_id_t candidate_id, 
				log_index_t last_log_index,
				term_t last_log_term);

		virtual void send_append_entries(
				term_t leader_term,
				server_id_t leader_id,
				log_index_t prev_log_index,
				term_t prev_log_term,
				vector<log_entry_t> entries,
				log_index_t commit_index);

};
#endif
