#include "test_observer.h"
#include "simulator.h"
#include <iostream>

using namespace std;
using namespace sim;

test_observer::test_observer() 
	: m_last_printed_time(0) {

}

void
test_observer::observe(simulator* sim) {
	if(m_last_printed_time + 1000 <= sim->time()) {
		cout<<"Observer: time="<<sim->time()<<endl;
		m_last_printed_time = sim->time();
	}
}
