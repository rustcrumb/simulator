#include <iostream>
#include "test_actor.h"
#include "sim_types.h"

using namespace std;
using namespace sim;

const sim_event_t FREQ = 1;

test_actor::test_actor(string id, sim_duration_t freq) 
	: m_id(id), m_freq(freq) {

}

void
test_actor::start(simulator* sim) {
	sim->schedule_event((iactor*)this, m_freq, FREQ);
}

void
test_actor::handle_event(sim_event_t type, simulator* sim) {
	switch(type) {
		case FREQ:
			//cout<<m_id<<": Received event time="<<sim->time()<<endl;
			sim->schedule_event((iactor*)this, m_freq, FREQ);
			break;
		default:
			cerr<<"Unknown event type: "<<type<<endl;
	}
}

