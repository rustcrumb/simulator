#include "simulator.h"
#include "sim_types.h"

namespace sim {

simulator::simulator() 
	:m_time(0) {

}

void
simulator::add_actor(iactor* actor) {
	m_actors.push_back(actor);
}

void
simulator::add_observer(iobserver* observer) {
	m_observers.push_back(observer);
}

void
simulator::run_until(sim_time_t time) {
	for(int i = 0, n = m_actors.size(); i < n; ++i) {
		m_actors[i]->start(this);
	}

	m_time = 0;
	while(!m_events.empty()) {
		const sim_event& top = m_events.top();
		if(top.time() > time) {
			break;
		}

		m_time = top.time();
		top.actor()->handle_event(top.type(), this);

		for(int i = 0, n = m_observers.size(); i < n; ++i) {
			m_observers[i]->observe(this);
		}

		m_events.pop();
	}
}

sim_time_t
simulator::time() const{
	return m_time;
}

void
simulator::schedule_event(iactor* actor, sim_duration_t duration, sim_event_t type) {
	m_events.push(sim_event(actor, m_time + duration, type));
}

}
