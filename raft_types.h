#ifndef RAFT_TYPES_H
#define RAFT_TYPES_H

namespace raft {
typedef unsigned int term_t;
typedef unsigned int server_id_t;
typedef unsigned long server_time_t;
typedef unsigned long server_duration_t;
typedef unsigned int log_index_t;

typedef struct {
	term_t term;
	bool vote_granted;
} request_vote_response_t;

typedef struct {
	term_t term;
	bool success;
} append_entries_response_t;

typedef struct {
	term_t term;
	log_index_t index;
} log_entry_t;

}

#endif
